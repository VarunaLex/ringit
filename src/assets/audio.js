/**
 * Helper function to emit a beep sound in the browser using an Audio File.
 *
 * @param {number} volume - The volume of the beep sound.
 *
 * @returns {Promise} - A promise that resolves when the beep sound is finished.
 */
const ringTheBell = (volume) => {
	return new Promise((resolve, reject) => {
		volume = volume || 100;

		try {
			// You're in charge of providing a valid AudioFile that can be reached by your web app
			let soundSource = './assets/bell.mp3';
			let sound = new Audio(soundSource);

			// Set volume
			sound.volume = volume / 100;

			sound.onended = () => {
				resolve();
			};

			sound.play();
		} catch (error) {
			reject(error);
		}
	});
};

const delay = (duration) => {
	return new Promise((resolve) => {
		setTimeout(() => resolve(), duration);
	});
};

window.ringTheBell = ringTheBell;
window.delay = delay;

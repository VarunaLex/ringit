const _sessions = 21;
// Check if local storage key exist
if (typeof localStorage.getItem('bell-data') == 'undefined' || localStorage.getItem('bell-data') == null) {
	// Create dummy storage
	let fake_data = [];
	for (let index = 1; index < _sessions; index++) {
		fake_data.push({
			id: index,
			note: '-',
			time: '',
			sl: 'S',
			rang: 'false',
		});
	}
	localStorage.setItem('bell-data', JSON.stringify(fake_data));
}

let getData = JSON.parse(localStorage.getItem('bell-data'));
for (let index = 1; index < _sessions; index++) {
	// Array Index
	let ai = index - 1;
	// Append form data
	let is_selected_S = getData[ai].sl == 'S' ? 'selected' : '';
	let is_selected_L = getData[ai].sl == 'L' ? 'selected' : '';
	let is_rang = getData[ai].rang == 'true' ? '✅' : '-';
	let uid = getData[ai].id;
	$('#editable>tbody').append(`
      <tr>
          <td><input type="hidden" id="b-id-${uid}" name="b-id-${uid}" value="${uid}">${uid}</td>
          <td><input type="text" id="b-note-${uid}" name="b-note-${uid}" value="${getData[ai].note}"></td>
          <td><input type="time" id="b-time-${uid}" name="b-time-${uid}" value="${getData[ai].time}"></td>
          <td><select id="b-sl-${uid}" name="b-sl-${uid}">
            <option value="S" ${is_selected_S}>S</option>
            <option Value="L" ${is_selected_L}>L</option>
            </select></td>
            <td><input type="hidden" id="b-rang-${uid}" name="b-rang-${uid}" value="${getData[ai].rang}">${is_rang}</td>
        </tr>
        `);
}

// Save & Update
$('#bell-schedule').submit((event) => {
	event.preventDefault();

	let data = [];
	for (let index = 1; index < _sessions; index++) {
		data.push({
			id: $('#b-id-' + index).val(),
			note: $(`#b-note-${index}`).val(),
			time: $('#b-time-' + index).val(),
			sl: $('#b-sl-' + index).val(),
			rang: $('#b-rang-' + index).val(),
		});
	}

	(async function () {
		if (await confirm('Are you sure to update the schedule?')) {
			localStorage.setItem('bell-data', JSON.stringify(data));
			location.reload();
		}
	})();
});

// Reset rings
$('#reset-rings').on('click', (e) => {
	let r_data = JSON.parse(localStorage.getItem('bell-data'));

	(async function () {
		if (await confirm('Are you sure to reset the rings?')) {
			for (let index = 1; index < _sessions; index++) {
				let ai = index - 1;
				r_data[ai].rang = 'false';
			}
			localStorage.setItem('bell-data', JSON.stringify(r_data));
			location.reload();
		}
	})();
});

// Split the time to remove starting 0
function filter_time(time) {
	let arr = time.split('');
	if (arr[0] == 0 || arr[0] == '0') {
		arr.shift();
		return arr.join('');
	}
	return time;
}

// Ringing Loop
setInterval(() => {
	var now = new Date();

	// Add leading 0
	let format_hours = String(now.getHours()).padStart(2, '0');
	let format_mins = String(now.getMinutes()).padStart(2, '0');

	now = format_hours + ':' + format_mins;

	let t_data = JSON.parse(localStorage.getItem('bell-data'));

	// Update ring status
	t_data.forEach((ele, i) => {
		if (ele.time == now.toString() && ele.rang == 'false') {
			t_data[i].rang = 'true';
			localStorage.setItem('bell-data', JSON.stringify(t_data));
			if (ele.sl == 'S') {
				$('#short-ring').trigger('click');
			} else if (ele.sl == 'L') {
				$('#long-ring').trigger('click');
			}
		}
	});
}, 10000);

$('#short-ring').on('click', () => {
	Promise.resolve()
		.then(() => ringTheBell())
		// .then(() => delay(1000))
		.then(() => location.reload());
});

$('#long-ring').on('click', () => {
	Promise.resolve()
		.then(() => ringTheBell())
		.then(() => ringTheBell())
		.then(() => ringTheBell())
		.then(() => location.reload());
});
